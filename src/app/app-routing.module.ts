import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./screens/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'create-game',
    loadChildren: () => import('./screens/create-game/create-game.module').then( m => m.CreateGamePageModule)
  },
  {
    path: 'game-arena',
    loadChildren: () => import('./screens/game-arena/game-arena.module').then( m => m.GameArenaPageModule)
  },
  {
    path: 'finish-game',
    loadChildren: () => import('./screens/finish-game/finish-game.module').then( m => m.FinishGamePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
