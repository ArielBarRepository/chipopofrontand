import { Player } from './player.model';

export class Game{
    GameDate:Date;
    HandCost:number;
    Players:Array<Player>;
}