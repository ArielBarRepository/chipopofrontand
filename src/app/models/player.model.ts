export class Player{
    Id:number;
    PlayerName:string;
    NumberOfBoughtHands : number = 1;
    NumberOfReturnedToJackpotHands : number;
    CalculateMoney:number;
}