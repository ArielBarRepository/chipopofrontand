import { PickerOptions } from '@ionic/core';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PickerController } from '@ionic/angular';

@Component({
  selector: 'cost-hand-picker',
  templateUrl: './cost-hand-picker.component.html',
  styleUrls: ['./cost-hand-picker.component.scss'],
})
export class CostHandPicker implements OnInit {

  @Output() valueSelected: EventEmitter<any> = new EventEmitter<number>();

  constructor(private pickerController: PickerController) { }

  ngOnInit() {
    this.createHandCostOptions();
  }
  createHandCostOptions() {
    for (let index = 10; index <= 100; index += 10) {
      this.handCostOptions.push(index);
    }
    for (let index = 150; index <= 500; index += 50) {
      this.handCostOptions.push(index);
    }
    for (let index = 600; index <= 1000; index += 100) {
      this.handCostOptions.push(index);
    }
  }

  handCostOptions: number[] = [];

  async showPicker() {
    let options: PickerOptions = {
      buttons: [
        {
          text: "Cancel",
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: (selected: any) => {
            this.valueSelected.emit(selected.handCost.value);
          }
        }
      ],
      columns: [{
        name: 'handCost',
        options: this.getColumnOptions()
      }]
    };

    let picker = await this.pickerController.create(options);
    picker.present()
  }

  getColumnOptions() {
    let options = [];
    this.handCostOptions.forEach(x => {
      options.push({ text: x, value: x });
    });
    return options;
  }
}
