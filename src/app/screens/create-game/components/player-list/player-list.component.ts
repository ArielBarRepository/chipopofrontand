import { Player } from './../../../../models/player.model';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss'],
})
export class PlayerListComponent implements OnInit {

  @Input('parentForm') gameForm : FormGroup;
  @ViewChild('editPlayer') editPlayer;
  currentPlayerIndexEdit: number = -1;
  currentIdexSequence : number = 1;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() { 
    this.addPlayer();
  }

  getPlayers() {
    return (<FormArray>this.gameForm.get('Players'));
  }

  addPlayer() {
    let newItem: FormGroup = this.initPlayer();
    (this.getPlayers()).push(newItem);
    this.currentPlayerIndexEdit = this.getPlayers().length - 1;
    setTimeout(() => {
      this.editPlayer.setFocus();
    }, 150);
  }

  initPlayer() {
    let newItem: FormGroup = this.formBuilder.group({
      Id: [this.currentIdexSequence, Validators.required],
      PlayerName: ['', Validators.required]
    });
    this.currentIdexSequence++;
    return newItem;
  }

  OnEditPlayerBlur() {
    if (!(<Player>this.getPlayers().value[this.currentPlayerIndexEdit]).PlayerName) {
      this.removePlayerByIndex(this.currentPlayerIndexEdit);
      this.currentPlayerIndexEdit = -1;
    }
    else {
      this.addPlayer();
    }
  }

  removePlayerByIndex(index: number) {
    this.getPlayers().controls.splice(index, 1);
    this.getPlayers().updateValueAndValidity();
  }

}
