import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';

import { CreateGamePageRoutingModule } from './create-game-routing.module';
import { CreateGamePage } from './create-game.page';
import { CostHandPicker } from './components/cost-hand-picker/cost-hand-picker.component';
import { PlayerListComponent } from './components/player-list/player-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CreateGamePageRoutingModule,
    MatStepperModule,
    MatIconModule
  ],
  declarations: [CreateGamePage,CostHandPicker, PlayerListComponent]
})
export class CreateGamePageModule {}

