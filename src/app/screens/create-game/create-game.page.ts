import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, PickerController } from '@ionic/angular';
import { PickerOptions } from "@ionic/core";
import { Player, Game } from 'src/app/models';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { GameService } from 'src/app/store/game-store/game.service';

@Component({
  selector: 'create-game',
  templateUrl: './create-game.page.html',
  styleUrls: ['./create-game.page.scss'],
})
export class CreateGamePage implements OnInit {

  gameForm: FormGroup;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder, private gameService: GameService) { }

  ngOnInit() {
    this.buildGameForm();
  }

  buildGameForm() {
    this.gameForm = this.formBuilder.group({
      GameDate: [Date.now()],
      HandCost: [20, Validators.required],
      Players: this.formBuilder.array([], [Validators.required, Validators.minLength(4)]),
    });
  }


  pop() {
    this.navCtrl.navigateForward('');
  }

  goToGameArenaPage() {
    this.navCtrl.navigateForward('game-arena');
  }

  createGame() {
    if (this.gameForm.valid) {
      const game: Game = this.gameForm.value;
      game.Players.forEach(player => {
        player.NumberOfBoughtHands = 1;
      });
      this.gameService.createGame(game);
      this.goToGameArenaPage();
    }
  }
}
