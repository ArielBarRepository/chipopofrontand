import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinishGamePageRoutingModule } from './finish-game-routing.module';

import { FinishGamePage } from './finish-game.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinishGamePageRoutingModule
  ],
  declarations: [FinishGamePage]
})
export class FinishGamePageModule {}
