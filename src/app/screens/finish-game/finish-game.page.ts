import { Paying } from './../../models/paying.model';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Game, Player } from 'src/app/models';
import { NavController } from '@ionic/angular';
import { GameService } from 'src/app/store/game-store/game.service';

@Component({
  selector: 'app-finish-game',
  templateUrl: './finish-game.page.html',
  styleUrls: ['./finish-game.page.scss'],
})
export class FinishGamePage implements OnInit {

  winPlayers$:Observable<Array<Player>>;
  losePlayers$:Observable<Array<Player>>;
  evenPlayers$:Observable<Array<Player>>;

  payingList$:Observable<Array<Paying>>;

  constructor(public navCtrl: NavController, private gameService: GameService) { }

  ngOnInit() {
    this.winPlayers$ = this.gameService.getWinPlayers();
    this.losePlayers$ = this.gameService.getLosePlayers();
    this.evenPlayers$ = this.gameService.getEvenPlayers();

    this.payingList$ = this.gameService.getPayingList();
  }

}
