import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Player } from 'src/app/models';

@Component({
  selector: 'player-hands',
  templateUrl: './player-hands.component.html',
  styleUrls: ['./player-hands.component.scss'],
})
export class PlayerHandsComponent implements OnInit {

  @Input('player') player: Player;
  @Output() decreaseClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() increaseClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() returnHandSelected: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() { }
}
