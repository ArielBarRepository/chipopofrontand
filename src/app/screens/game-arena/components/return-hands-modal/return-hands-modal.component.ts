import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-return-hands-modal',
  templateUrl: './return-hands-modal.component.html',
  styleUrls: ['./return-hands-modal.component.scss'],
})
export class ReturnHandsModal implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

}
