import { PickerOptions } from '@ionic/core';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { PickerController } from '@ionic/angular';

@Component({
  selector: 'return-hands-picker',
  templateUrl: './return-hands-picker.component.html',
  styleUrls: ['./return-hands-picker.component.scss'],
})
export class ReturnHandsPicker implements OnInit {

  @Input('numberOfReturnedToJackpotHands') value = 1;
  @Output() valueSelected: EventEmitter<any> = new EventEmitter<number>();

  constructor(private pickerController: PickerController) { }

  ngOnInit() {
    this.createHandCostOptions();
  }
  createHandCostOptions() {
    for (let index = 0; index <= 30; index++) {
      this.returnHandOptions.push(index);
    }
  }

  returnHandOptions: number[] = [];

  async showPicker() {
    let options: PickerOptions = {
      buttons: [
        {
          text: "Cancel",
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: (selected: any) => {
            this.valueSelected.emit(selected.returnHand.value);
          }
        }
      ],
      columns: [{
        name: 'returnHand',
        options: this.getColumnOptions(),
        selectedIndex: this.value
      }]
    };

    let picker = await this.pickerController.create(options);
    picker.present()
  }

  getColumnOptions() {
    let options = [];
    this.returnHandOptions.forEach(x => {
      options.push({ text: x, value: x });
    });
    return options;
  }
}
