import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameArenaPage } from './game-arena.page';

const routes: Routes = [
  {
    path: '',
    component: GameArenaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GameArenaPageRoutingModule {}
