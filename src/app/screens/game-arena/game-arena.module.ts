import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GameArenaPageRoutingModule } from './game-arena-routing.module';

import { GameArenaPage } from './game-arena.page';
import { CalcJackpotPipe } from './pipes/calc-jackpot.pipe';
import { PlayerHandsComponent } from './components/player-hands/player-hands.component';
import { ReturnHandsPicker } from './components/return-hands-picker/return-hands-picker.component';
import { CalcTotalReturnHandsPipe } from './pipes/calc-total-return-hands.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GameArenaPageRoutingModule
  ],
  declarations: [GameArenaPage, PlayerHandsComponent, CalcJackpotPipe, ReturnHandsPicker, CalcTotalReturnHandsPipe]
})
export class GameArenaPageModule {}
