import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Game, Player } from 'src/app/models';
import { NavController } from '@ionic/angular';
import { GameService } from 'src/app/store/game-store/game.service';

@Component({
  selector: 'app-game-arena',
  templateUrl: './game-arena.page.html',
  styleUrls: ['./game-arena.page.scss'],
})
export class GameArenaPage implements OnInit {

  gameDetails$:Observable<Game>;

  constructor(public navCtrl: NavController, private gameService: GameService) { }

  ngOnInit() {
    this.gameDetails$ = this.gameService.getGameDetails();
  }

  handsNumberChanged(player : Player,updatePlayer : Player){
    player = updatePlayer;
  }

  onDecreaseClicked(playerId:number){
    this.gameService.decreasePlayerBoughtHand(playerId);
  }

  onIncreaseClicked(playerId:number){
    this.gameService.increasePlayerBoughtHand(playerId);
  }

  onReturnHandSelected(playerId:number,returnHand:number){
    this.gameService.updatePlayerReturnHands(playerId, returnHand);
  }

  goToFinishGamePage() {
    this.navCtrl.navigateForward('finish-game');
  }

  finishGame(){
    this.gameService.finishGame();
    this.goToFinishGamePage();
  }
}
