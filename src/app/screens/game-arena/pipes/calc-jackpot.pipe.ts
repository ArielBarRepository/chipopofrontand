import { Pipe, PipeTransform } from '@angular/core';
import { Game } from 'src/app/models';

@Pipe({
  name: 'calcJackpot'
})
export class CalcJackpotPipe implements PipeTransform {

  transform(game: Game): number {
    let jackpot: number = 0;
    if (game) {
      game.Players.forEach(player => {
        jackpot += player.NumberOfBoughtHands * game.HandCost;
      });
    }
    return jackpot;
  }

}
