import { Pipe, PipeTransform } from '@angular/core';
import { Game } from 'src/app/models';

@Pipe({
  name: 'calcTotalReturnHands'
})
export class CalcTotalReturnHandsPipe implements PipeTransform {

  transform(game: Game): number {
    let jackpot: number = 0;
    if (game) {
      game.Players.forEach(player => {
        jackpot += (player.NumberOfReturnedToJackpotHands ? player.NumberOfReturnedToJackpotHands : 0) * game.HandCost;
      });
    }
    return jackpot;
  }

}
