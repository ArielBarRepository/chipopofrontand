import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  animateOn : boolean = false;
  constructor( public navCtrl: NavController) {}

  createNewGame(){
    this.animateOn = true;
    setTimeout(() => {
      this.navCtrl.navigateForward('create-game');
      this.animateOn = false;
    }, 1000);
  }
}
