import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducer } from './game.reducer';
import { EffectsModule } from '@ngrx/effects';
import { GameEffects } from './game.effects';



@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('game', reducer),
    EffectsModule.forFeature([GameEffects])
  ],
  providers: [GameEffects]
})
export class GameStoreModule { }
