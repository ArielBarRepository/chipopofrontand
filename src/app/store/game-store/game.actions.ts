import { Game } from './../../models/game.model';
import { Action } from '@ngrx/store';

export enum ActionTypes {
  DecreasePlayerBoughtHand = '[Game] Decrease Player Bought Hand',
  IncreasePlayerBoughtHand = '[Game] Increase Player Bought Hand',
  UpdatePlayerReturnHands = '[Game] Update Player Return Hands',
  FinishGame = '[Game] Finish Game',

  SetGameToStore = '[Game] Set Game To Store',
}

export class DecreasePlayerBoughtHandAction implements Action {
  readonly type = ActionTypes.DecreasePlayerBoughtHand;

  constructor(public paylod: { playerId: number }) {
  }
}

export class IncreasePlayerBoughtHandAction implements Action {
  readonly type = ActionTypes.IncreasePlayerBoughtHand;

  constructor(public paylod: { playerId: number }) {
  }
}

export class UpdatePlayerReturnHandsAction implements Action {
  readonly type = ActionTypes.UpdatePlayerReturnHands;

  constructor(public paylod: { playerId: number,returnHands : number }) {
  }
}

export class FinishGameAction implements Action {
  readonly type = ActionTypes.FinishGame;

  constructor() {
  }
}

//reducer actions
export class SetGameToStoreAction implements Action {
  readonly type = ActionTypes.SetGameToStore;

  constructor(public paylod: { game: Game }) {
  }
}


export type GameActions = SetGameToStoreAction
| DecreasePlayerBoughtHandAction
| IncreasePlayerBoughtHandAction
| UpdatePlayerReturnHandsAction
| FinishGameAction;
