import { Game } from './../../models/game.model';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects'
import { catchError, map, switchMap, mergeMap, withLatestFrom } from 'rxjs/operators';
import { Action, Store } from '@ngrx/store';
import { Observable, of as observableOf } from 'rxjs';

import { GameService } from './game.service';
import * as gameActions from './game.actions';
import * as gameSelectors from './game.selectors';
import { AppState } from '..';


@Injectable()
export class GameEffects {
  constructor(private gameService: GameService, private actions$: Actions, private store: Store<AppState>) { }

  // @Effect()
  // DecreasePlayerBoughtHandAction$ = this.actions$.pipe(
  //   ofType<gameActions.DecreasePlayerBoughtHandAction>(
  //     gameActions.ActionTypes.DecreasePlayerBoughtHand
  //   ),
  //   withLatestFrom(this.store.select(gameSelectors.getGameDetails)),
  //   switchMap((action, gameDetails) => {
  //     [
  //       new gameActions.SetGameToStoreAction({ game: gameDetails })
  //     ]
  //   }
  //   ),
  //   catchError(error =>
  //     observableOf(null)
  //   )
  // );

}
