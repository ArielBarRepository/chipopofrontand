import { cloneDeep } from 'lodash';
import { Action } from '@ngrx/store';
import { Game } from './../../models/game.model';
import * as gameActions from './game.actions';


export const ordersFeatureKey = 'orders';

export interface State {
  gameDetails: Game
}

export const initialState: State = {
  gameDetails: null
};

export function reducer(state = initialState, action: gameActions.GameActions): State {
  switch (action.type) {
    case gameActions.ActionTypes.SetGameToStore:
      return { ...state, gameDetails: action.paylod.game };
    case gameActions.ActionTypes.DecreasePlayerBoughtHand:
      return decreasePlayerBoughtHand(state, action.paylod.playerId);
    case gameActions.ActionTypes.IncreasePlayerBoughtHand:
      return increasePlayerBoughtHand(state, action.paylod.playerId);
    case gameActions.ActionTypes.UpdatePlayerReturnHands:
      return UpdatePlayerReturnHands(state, action.paylod.playerId, action.paylod.returnHands);
    case gameActions.ActionTypes.FinishGame:
      return FinishGame(state);
    default:
      return state;
  }
}

function decreasePlayerBoughtHand(state: State, playerId: number) {
  const game = cloneDeep(state.gameDetails);
  game.Players.forEach(player => {
    if (player.Id == playerId && player.NumberOfBoughtHands > 1) {
      player.NumberOfBoughtHands--;
    }
  });
  return { ...state, gameDetails: game };
}

function increasePlayerBoughtHand(state: State, playerId: number) {
  const game = cloneDeep(state.gameDetails);
  game.Players.forEach(player => {
    if (player.Id == playerId) {
      player.NumberOfBoughtHands++;
    }
  });
  return { ...state, gameDetails: game };
}

function UpdatePlayerReturnHands(state: State, playerId: number, returnHands: number) {
  const game = cloneDeep(state.gameDetails);
  game.Players.forEach(player => {
    if (player.Id == playerId) {
      player.NumberOfReturnedToJackpotHands = returnHands;
    }
  });
  return { ...state, gameDetails: game };
}

function FinishGame(state: State) {
  const game = cloneDeep(state.gameDetails);
  game.Players.forEach(player => {
    player.CalculateMoney = (player.NumberOfReturnedToJackpotHands - player.NumberOfBoughtHands) * game.HandCost;
  });
  return { ...state, gameDetails: game };
}
