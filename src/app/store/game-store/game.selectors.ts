import * as fromGame from './game.reducer';
import { createFeatureSelector, createSelector } from "@ngrx/store";
import { cloneDeep } from 'lodash';
import { Paying } from 'src/app/models';

export const getGameFeatureState = createFeatureSelector<fromGame.State>('game');
export const getGameDetails = createSelector(getGameFeatureState, state => cloneDeep(state.gameDetails));

export const getWinPlayers = createSelector(getGameFeatureState, state => {
    const winPlayers = state.gameDetails.Players.filter(p => p.CalculateMoney > 0);
    return winPlayers;
});

export const getLosePlayers = createSelector(getGameFeatureState, state => {
    const losePlayers = state.gameDetails.Players.filter(p => p.CalculateMoney < 0);
    return losePlayers;
});

export const getEvenPlayers = createSelector(getGameFeatureState, state => {
    const evenPlayers = state.gameDetails.Players.filter(p => p.CalculateMoney == 0);
    return evenPlayers;
});

export const getPayingList = createSelector(getGameFeatureState, state => {
    let winners = cloneDeep(state.gameDetails.Players.filter(p => p.CalculateMoney > 0));
    let losers = cloneDeep(state.gameDetails.Players.filter(p => p.CalculateMoney < 0));

    const payingList: Array<Paying> = new Array<Paying>();

    let winnersIdxToRemove: Array<number> = new Array<number>();
    for (let index = 0; index < winners.length; index++) {
        const matchIdx = losers.findIndex(l => (l.CalculateMoney * -1) == winners[index].CalculateMoney);
        if (matchIdx != -1) {
            payingList.push({ FromPlayer: losers[matchIdx], ToPlayer: winners[index], howMatch: winners[index].CalculateMoney });
            losers.splice(matchIdx, 1);
            winnersIdxToRemove.push(index);
        }
    }

    winnersIdxToRemove.forEach(i => {
        winners.splice(i, 1);
    });

    winnersIdxToRemove = new Array<number>();
    for (let index = 0; index < winners.length; index++) {
        const match = losers.filter(l => (l.CalculateMoney * -1) < winners[index].CalculateMoney);
        match.forEach(loser => {
            if (winners[index].CalculateMoney > (loser.CalculateMoney * -1)) {
                payingList.push({ FromPlayer: loser, ToPlayer: winners[index], howMatch: (loser.CalculateMoney * -1) });
                const matchIdx = losers.findIndex(l => l.Id == loser.Id);
                if (matchIdx != -1) {
                    losers.splice(matchIdx, 1);
                }
                winners[index].CalculateMoney += loser.CalculateMoney;
            }
            if (winners[index].CalculateMoney == 0) {
                winnersIdxToRemove.push(index);
            }
        });
    }

    winnersIdxToRemove.forEach(i => {
        winners.splice(i, 1);
    });

    let losersIdxToRemove = new Array<number>();
    for (let index = 0; index < losers.length; index++) {
        const match = winners.filter(l => (losers[index].CalculateMoney * -1) > l.CalculateMoney);
        match.forEach(winner => {
            if ((losers[index].CalculateMoney * -1) > winner.CalculateMoney) {
                payingList.push({ FromPlayer: losers[index], ToPlayer: winner, howMatch: winner.CalculateMoney });
                const matchIdx = winners.findIndex(l => l.Id == winner.Id);
                if (matchIdx != -1) {
                    winners.splice(matchIdx, 1);
                }
                losers[index].CalculateMoney += winner.CalculateMoney;
            }
            if (losers[index].CalculateMoney == 0) {
                losersIdxToRemove.push(index);
            }
        });
    }

    losersIdxToRemove = new Array<number>();
    for (let index = 0; index < losers.length; index++) {
        const matchIdx = winners.findIndex(l => l.CalculateMoney == (losers[index].CalculateMoney * -1));
        if (matchIdx != -1) {
            payingList.push({ FromPlayer: losers[index], ToPlayer: winners[matchIdx], howMatch: (losers[index].CalculateMoney * -1) });
            winners.splice(matchIdx, 1);
            losersIdxToRemove.push(index);
        }
    }

    losersIdxToRemove.forEach(i => {
        losers.splice(i, 1);
    });

    return payingList;
});