import { Paying } from './../../models/paying.model';
import { Injectable } from '@angular/core';
import { State } from './game.reducer';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import * as gameActions from './game.actions';
import * as gameSelectors from './game.selectors';
import { Game } from 'src/app/models/game.model';
import { Player } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(protected store: Store<State>) { }

  public createGame(game: Game) {
    this.store.dispatch(new gameActions.SetGameToStoreAction({ game: game }));
  }

  public decreasePlayerBoughtHand(playerId: number) {
    this.store.dispatch(new gameActions.DecreasePlayerBoughtHandAction({ playerId: playerId }));
  }

  public increasePlayerBoughtHand(playerId: number) {
    this.store.dispatch(new gameActions.IncreasePlayerBoughtHandAction({ playerId: playerId }));
  }

  public updatePlayerReturnHands(playerId: number, returnHands: number) {
    this.store.dispatch(new gameActions.UpdatePlayerReturnHandsAction({ playerId: playerId, returnHands: returnHands }));
  }

  public finishGame() {
    this.store.dispatch(new gameActions.FinishGameAction());
  }

  //selectors
  public getGameDetails(): Observable<Game> {
    return this.store.select(gameSelectors.getGameDetails);
  }

  public getWinPlayers(): Observable<Array<Player>> {
    return this.store.select(gameSelectors.getWinPlayers);
  }
  public getLosePlayers(): Observable<Array<Player>> {
    return this.store.select(gameSelectors.getLosePlayers);
  }
  public getEvenPlayers(): Observable<Array<Player>> {
    return this.store.select(gameSelectors.getEvenPlayers);
  }

  public getPayingList(): Observable<Array<Paying>> {
    return this.store.select(gameSelectors.getPayingList);
  }

}
