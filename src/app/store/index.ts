import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from 'src/environments/environment';

import * as fromGame from './game-store/game.reducer'

export interface AppState {
    game: fromGame.State,
}

export const reducers: ActionReducerMap<AppState> = {
    game: fromGame.reducer,
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [storeFreeze] : [];